#!/bin/env python

import jsonmerge as jm

def main():
    jm.merge()

if __name__ == "__main__":
    main()