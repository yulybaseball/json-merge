from os import path
import datetime
import sys
import json

from readjavaprop import Properties

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
CONF_FILE_NAME = path.join(path.dirname(EXEC_DIR), 'ypu-config.properties')
LANGUAGES = ['EN_US', 'ES_ES']
ORIG_JSON_PATTERN = 'ORIG_{0}_JSON'
SF_JSON_PATTERN = 'SF_{0}_JSON'
MERGED_JSON_PATTERN = 'MERGED_{0}_JSON'

def _get_json_files(language):
    """Return the path of both json files that need to be merged."""
    jpr = Properties()
    jpr.load(open(CONF_FILE_NAME))
    origjsonf = jpr[ORIG_JSON_PATTERN.format(language)]
    sfjsonf = jpr[SF_JSON_PATTERN.format(language)]
    print "Orig JSON file: {0}".format(origjsonf)
    print "SF JSON file: {0}".format(sfjsonf)
    return (origjsonf, sfjsonf)

def _update_json(json_data, language):
    jpr = Properties()
    jpr.load(open(CONF_FILE_NAME))
    try:
        mjsonf = jpr[MERGED_JSON_PATTERN.format(language)]
        with open(mjsonf, 'w+') \
                as json_data_file:
            json_data_file.write(json.dumps(json_data))
            print "Merged file was saved in {0}".format(mjsonf)
    except Exception as e:
        print "Error creating the merged JSON: {0}".format(str(e))

def merge():
    try:
        for language in LANGUAGES:
            orig_file, sf_file = _get_json_files(language)
            if orig_file and sf_file:
                orig_file_json = json.load(open(orig_file))
                sf_file_json = json.load(open(sf_file))
                merged_json = orig_file_json.copy()
                merged_json.update(sf_file_json)
                _update_json(merged_json, language)
                print ("Merged JSON created for language {0}".format(language))
            else:
                print "Error reading one or both JSON files for language {0}.".\
                        format(language)
                exit() 
    except Exception as e:
        print "There was an error: {0}".format(str(e))
        print "Script will exit now."
        exit()

